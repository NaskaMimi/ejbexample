<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c' %>
<html>
<head title="Список пользователей">
    <script>
        function showUpdateForm(formId) {
            document.getElementById("updateForm" + formId).hidden = false;
            document.getElementById("cancelButton" + formId).hidden = false;
            document.getElementById("deleteForm" + formId).hidden = false;
            document.getElementById("updateButton" + formId).hidden = true;
        }

        function hideUpdateForm(formId) {
            document.getElementById("updateForm" + formId).hidden = true;
            document.getElementById("cancelButton" + formId).hidden = true;
            document.getElementById("deleteForm" + formId).hidden = true;
            document.getElementById("updateButton" + formId).hidden = false;
        }
    </script>
    <style>
        .updating-form {
            display: flex;
            flex-direction: row;
            justify-content: flex-start;
        }
    </style>
</head>
<body>

<form method="post" action="saveServlet">
    Name: <input name="name"/><br/>
    Age: <input name="age"/><br/>
    <button>Submit</button>
</form>

<br>
<h1>Список пользователей</h1>
<table>
    <tr>
        <td>Номер</td>
        <td>Имя</td>
        <td>Возраст</td>
    </tr>
    <c:forEach items="${users}" var="user">
        <tr>
            <td>${user.id}</td>
            <td>${user.name}</td>
            <td>${user.age}</td>
            <td>
                <div class="updating-form">
                    <button id="updateButton${user.id}" onclick="showUpdateForm(${user.id})">Редактировать</button>
                    <form id="updateForm${user.id}" method="post" action="updateServlet"
                                 hidden="true">
                        Id: <input name="id" value="${user.id}"/>
                        Name: <input name="name" value="${user.name}"/>
                        Age: <input name="age" value="${user.age}"/>
                        <button>Применить</button>
                    </form>
                    <button id="cancelButton${user.id}" onclick="hideUpdateForm(${user.id})" hidden="true">Отмена</button>
                    <form id="deleteForm${user.id}" method="post" action="deleteServlet" hidden="true">
                        <input name="id" hidden="true" value="${user.id}"/>
                        <button>Удалить</button>
                    </form>
                </div>
            </td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
