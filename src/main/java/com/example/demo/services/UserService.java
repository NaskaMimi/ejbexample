package com.example.demo.services;

import com.example.demo.entities.UsersEntity;

import javax.ejb.Local;
import java.util.List;

@Local
public interface UserService {

    UsersEntity find(Object id);
    List<UsersEntity> findAll();
    void save(UsersEntity user);
    void update(UsersEntity user);
    void delete(Long userId);
}
