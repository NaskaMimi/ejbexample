package com.example.demo.services;

import com.example.demo.entities.UsersEntity;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
@LocalBean
public class UserServiceBean implements UserService {

    @PersistenceContext(unitName = "my-persistence-unit")
    private EntityManager entityManager;

    public UsersEntity find(Object id) {
        return entityManager.find(UsersEntity.class, id);
    }

    public List<UsersEntity> findAll() {
        return entityManager.createNamedQuery("UsersEntity.findAll", UsersEntity.class).getResultList();
    }

    public void save(UsersEntity user) {
        entityManager.persist(user);
    }

    public void update(UsersEntity user) {
        entityManager.merge(user);
    }

    public void delete(Long userId) {
        UsersEntity user = entityManager.find(UsersEntity.class, userId);
        entityManager.remove(user);
    }

}
