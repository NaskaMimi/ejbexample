package com.example.demo.controllers;

import com.example.demo.entities.UsersEntity;
import com.example.demo.services.UserService;

import javax.ejb.EJB;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/saveServlet")
public class SaveServlet extends HttpServlet {

    @EJB
    private UserService userService;

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");

        UsersEntity newUser = new UsersEntity();
        newUser.setName(request.getParameter("name"));
        newUser.setAge(Integer.valueOf(request.getParameter("age")));

        userService.save(newUser);
        response.sendRedirect(request.getContextPath() + "/helloServlet");
    }

}
