package com.example.demo.controllers;

import com.example.demo.entities.UsersEntity;
import com.example.demo.services.UserService;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/helloServlet")
public class HelloServlet extends HttpServlet {

    @EJB
    private UserService userService;

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");

        List<UsersEntity> users = userService.findAll();
        String dPage = "/WEB-INF/hello.jsp";

        request.setAttribute("users", users);

        try {
            getServletContext().getRequestDispatcher(dPage).forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        }

    }
}